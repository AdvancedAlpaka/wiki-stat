package victor1234544.wiki

import com.apurebase.arkenv.Arkenv
import com.apurebase.arkenv.util.argument
import com.apurebase.arkenv.util.parse
import kotlinx.datetime.*
import java.nio.file.Path
import java.text.SimpleDateFormat
import kotlin.io.path.exists
import kotlin.io.path.isReadable
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

class Parameters : Arkenv() {
    val inputs: List<Path> by argument("--inputs") {
        description = "Path(s) to bzip2 archived XML file(s) with WikiMedia dump. Comma separated."
        defaultValue = { listOf() }
        mapping = {
            it.split(",").map { name -> Path.of(name) }
        }
        validate("File does not exist") {
            it.all { file -> file.exists() && file.toFile().isFile }
        }
        validate("File cannot be read") {
            it.all { file -> file.toFile().isFile && file.isReadable() }
        }
    }

    val timeOut: Long by argument("--timeout") {
        mapping = {
            val time = SimpleDateFormat("hh:mm:ss").parse(it)
            10800 + time.time / 1000
        }
        defaultValue = { if (date == "") 600 else 3600 }
    }

    val threads: Int by argument("--threads") {
        description = "Number of threads"
        defaultValue = { if (inputs.isEmpty()) 10 else inputs.size + 1 }
        validate("Number of threads must be in 1..32") {
            it in 1..32
        }
    }

    val output: String by argument("--output") {
        description = "Report output file"
        defaultValue = { if (!isAdvancedReport && date == "") "statistics.txt" else "statistics.html" }
    }

    val isAdvancedReport: Boolean by argument("--advanced") {
        defaultValue = { false }
    }

    val date: String by argument("--date") {
        description = "Date"
        defaultValue = { "" }
        mapping = {
            var date =
                if (it == "latest") Clock.System.todayAt(TimeZone.currentSystemDefault())
                else SimpleDateFormat("yyyyMMdd").parse(it).toInstant().toKotlinInstant().toLocalDateTime(TimeZone.currentSystemDefault()).date
            date = LocalDate(date.year, date.month, (date.dayOfMonth - 1) / 20 * 20 + 1)
            "${date.year}${"%02d".format(date.monthNumber)}${"%02d".format(date.dayOfMonth)}"
        }
    }
}

@OptIn(ExperimentalTime::class)
fun main(args: Array<String>) {
    try {
        val parameters = Parameters().parse(args)

        if (parameters.help) {
            println(parameters.toString())
            return
        }

        val duration = measureTime {
            Solver(parameters).solve()
        }
        println("Time: ${duration.inWholeMilliseconds} ms")
    } catch (e: Exception) {
        println("Error! ${e.message}")
        throw e
    }
}
