package victor1234544.wiki

import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlinx.datetime.toLocalDateTime
import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler
import kotlin.math.log10
import kotlin.math.max

class SAXPars(private val report: Report) : DefaultHandler() {
    private val thisElement = mutableListOf<String>()
    private val bufferText: StringBuilder = StringBuilder()
    private val bufferTitle: StringBuilder = StringBuilder()

    private var year: Int? = null
    private var logSize: Int? = null

    companion object {
        val wordRegex = Regex("[а-яА-Я]{3,}")
    }

    @Throws(SAXException::class)
    override fun startElement(namespaceURI: String, localName: String, qName: String, atts: Attributes) {
        thisElement.add(qName)
        if (thisElement.joinToString("/") == "mediawiki/page/revision/text") {
            val bytes = atts.getValue("bytes").toLongOrNull()
            if (bytes != null) {
                logSize = log10(max(1.0, bytes.toDouble())).toInt()
            }
        }
    }

    @Throws(SAXException::class)
    override fun endElement(namespaceURI: String, localName: String, qName: String) {
        if (thisElement.joinToString("/") == "mediawiki/page") {
            if (year != null && logSize != null) {
                report.sizesText.merge(logSize!!, 1, Long::plus)
                report.minMaxSizes.add(logSize!!)

                report.yearsText.merge(year!!, 1, Long::plus)
                report.minMaxYears.add(year!!)

                wordRegex.findAll(bufferText).forEach {
                    report.wordsText.merge(it.value.lowercase(), 1, Long::plus)
                }
                wordRegex.findAll(bufferTitle).forEach {
                    report.wordsTitle.merge(it.value.lowercase(), 1, Long::plus)
                }
            }
            logSize = null
            year = null
            bufferText.clear()
            bufferTitle.clear()
        }
        thisElement.removeLast()
    }

    @Throws(SAXException::class)
    override fun characters(ch: CharArray, start: Int, length: Int) {
        ch.concatToString(start, start + length).also {
            when (thisElement.joinToString("/")) {
                "mediawiki/page/title" -> {
                    bufferTitle.append(it)
                }
                "mediawiki/page/revision/text" -> {
                    if (logSize != null) bufferText.append(it)
                }
                "mediawiki/page/revision/timestamp" -> {
                    year = it.toInstant().toLocalDateTime(TimeZone.UTC).year
                }
            }
        }
    }
}
