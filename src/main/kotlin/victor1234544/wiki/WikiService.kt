package victor1234544.wiki

import kotlinx.serialization.json.JsonElement
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Streaming

interface WikiService {
    @GET("ruwiki/{date}/dumpstatus.json")
    fun dump(@Path("date") date: String): Call<JsonElement>

    @Streaming
    @GET("ruwiki/{date}/{name}")
    fun downloadFile(@Path("date") date: String, @Path("name") filename: String): Call<ResponseBody>
}
