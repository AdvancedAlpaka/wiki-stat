package victor1234544.wiki

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import okhttp3.MediaType
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import retrofit2.Retrofit
import java.io.BufferedInputStream
import java.io.InputStream
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.Executors
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit
import javax.xml.parsers.SAXParserFactory
import kotlin.io.path.Path
import kotlin.io.path.inputStream
import kotlin.io.path.outputStream

class Solver(private val pars: Parameters) {
    private val isAdvanced = pars.date != ""
    private val outputStream = PrintWriter(Path(pars.output).outputStream(), true, Charsets.UTF_8)
    private val tasks = Executors.newFixedThreadPool(pars.threads)
    private val downloads = Files.createTempDirectory(".downloads")

    private fun analyze(it: InputStream, report: Report) {
        try {
            val bzIn = BZip2CompressorInputStream(BufferedInputStream(it))

            val factory = SAXParserFactory.newInstance()
            val parser = factory.newSAXParser()
            val saxp = SAXPars(report)
            parser.parse(bzIn, saxp)
            bzIn.close()
        } catch (e: Throwable) {
            println("Exception from thread : $e")
        }
    }

    private fun saveFile(input: InputStream, pathWhereYouWantToSaveFile: Path): Path {
        try {
            BufferedInputStream(input).copyTo(pathWhereYouWantToSaveFile.outputStream())
            return pathWhereYouWantToSaveFile
        } catch (e: Exception) {
            error("saveFile : $e")
        } finally {
            input.close()
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    fun solve() {
        val report = Report()
        if (isAdvanced) {
            val builder = Retrofit.Builder()
                .baseUrl("https://dumps.wikimedia.org/")
                .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
                .build()
            val wikiService = builder.create(WikiService::class.java)
            val sem = Semaphore(3)

            wikiService.dump(pars.date).execute().let {
                if (it.isSuccessful && it.code() == 200) {
                    it.body()!!.jsonObject["jobs"]!!.jsonObject["metacurrentdump"]!!.jsonObject["files"]!!.jsonObject.keys
                    .forEach {
                        tasks.submit {
                            sem.acquire()
                            val responseBody = wikiService.downloadFile(pars.date, it).execute()
                            if (responseBody.isSuccessful && responseBody.code() == 200) {
                                println(
                                    """Successful try $it : 
                                        Response : $responseBody
                                    """.trimIndent()
                                )
                                val file = saveFile(responseBody.body()!!.byteStream(), downloads.resolve(it))
                                sem.release()
                                println("Successful save $it")
                                analyze(file.inputStream(), report)
                                println("Successful parse $it")
                            } else {
                                sem.release()
                                println(
                                    """Unsuccessful try : 
                                        Response : ${responseBody.raw()}
                                    """.trimIndent()
                                )
                            }
                        }
                    }
                } else {
                    println("Uncorrected date ${pars.date} : $it")
                }
            }
        } else {
            pars.inputs.forEach {
                tasks.submit {
                    println("begin-${Thread.currentThread().name} $it")
                    analyze(it.inputStream(), report)
                    println("end-${Thread.currentThread().name} $it")
                }
            }
        }

        tasks.shutdown()
        tasks.awaitTermination(pars.timeOut, TimeUnit.SECONDS)

        if (isAdvanced || pars.isAdvancedReport) {
            report.printHTML(outputStream)
        } else {
            report.printString(outputStream)
        }
        outputStream.close()
    }
}
