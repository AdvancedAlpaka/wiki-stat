package victor1234544.wiki

import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import java.io.PrintWriter
import java.util.concurrent.ConcurrentHashMap

class MinMax {
    var range: IntRange? = null

    fun add(value: Int) {
        if (range == null) {
            range = value..value
            return
        }

        when {
            value < range!!.first -> {
                range = value..range!!.last
            }
            range!!.last < value -> {
                range = range!!.first..value
            }
        }
    }
}

class Report {
    val wordsTitle: ConcurrentHashMap<String, Long> = ConcurrentHashMap()
    val wordsText: ConcurrentHashMap<String, Long> = ConcurrentHashMap()
    val sizesText: ConcurrentHashMap<Int, Long> = ConcurrentHashMap()
    val yearsText: ConcurrentHashMap<Int, Long> = ConcurrentHashMap()
    var minMaxYears: MinMax = MinMax()
    var minMaxSizes: MinMax = MinMax()

    private fun <T : Comparable<T>, R : Comparable<R>> top300(map: Map<T, R>, lambda: (Pair<T, R>) -> Unit) {
        map.toList().sortedWith { p1, p2 ->
            when {
                p1.second < p2.second -> 1
                p1.second > p2.second -> -1
                else -> p1.first.compareTo(p2.first)
            }
        }.take(300).forEach(lambda)
    }

    private fun <R> sorted(default: R, range: IntRange, map: Map<Int, R>, lambda: (Pair<Int, R>) -> Unit) {
        range.forEach {
            val element = map.getOrDefault(it, default)
            lambda(it to element)
        }
    }

    fun printHTML(out: PrintWriter) {
        val sortedSizeText = sizesText.toList().sortedBy { p -> p.first }
        val sortedYearText = yearsText.toList().sortedBy { p -> p.first }

        out.appendHTML().html {
            lang = "ru"
            head {
                title("Kotlin Advanced")
                meta {
                    charset = "UTF-8"
                }
                script(type = ScriptType.textJavaScript, src = "https://www.gstatic.com/charts/loader.js") {}
                script(type = ScriptType.textJavaScript) {
                    unsafe {
                        raw(
                            """
google.charts.load('current', {
            packages: ['corechart', 'bar']
        });
        google.charts.setOnLoadCallback(drawMaterial);

        function drawMaterial() {
            let data = new google.visualization.DataTable();
            data.addColumn('number', 'log10');
            data.addColumn('number', 'Кол-во статей');

            data.addRows([${sortedSizeText.joinToString(",") { "[${it.first}, ${it.second}]" }}]);

            let options = {
                title: 'Распределение статей по размеру:',
                hAxis: {
                    title: '',
                    viewWindow: {
                        min: ${minMaxSizes.range?.first},
                        max: ${minMaxYears.range?.last}
                    }
                },
                vAxis: {
                    title: ''
                }
            };

            materialChart = new google.charts.Bar(document.getElementById('chartSizesText'));
            materialChart.draw(data, options);

            data = new google.visualization.DataTable();
            data.addColumn('string', 'Год');
            data.addColumn('number', 'Кол-во статей');

            data.addRows([${sortedYearText.joinToString(",") { "['${it.first}', ${it.second}]" }}]);

            options = {
                title: 'Распределение статей по годам:',
                hAxis: {
                    title: '',
                    viewWindow: {
                        min: ${minMaxYears.range?.first},
                        max: ${minMaxYears.range?.last}
                    }
                },
                vAxis: {
                    title: ''
                }
            };

            var materialChart = new google.charts.Bar(document.getElementById('chartYearText'));
            materialChart.draw(data, options);
}
                            """.trimIndent()
                        )
                    }
                }
            }
            body {
                div {
                    h3 { +"Топ-300 слов в заголовках статей:" }
                    top300(wordsTitle) {
                        p { +"${it.second} ${it.first}" }
                    }
                    h3 { +"Топ-300 слов в статьях:" }
                    top300(wordsText) {
                        p { +"${it.second} ${it.first}" }
                    }
                    h3 { +"Распределение статей по размеру:" }
                    div {
                        id = "chartSizesText"
                    }
                    h3 { +"Распределение статей по времени:" }
                    div {
                        id = "chartYearText"
                    }
                }
            }
        }
    }

    fun printString(out: PrintWriter) {
        out.println("Топ-300 слов в заголовках статей:")
        top300(wordsTitle) {
            out.println("${it.second} ${it.first}")
        }
        out.println()
        out.println("Топ-300 слов в статьях:")
        top300(wordsText) {
            out.println("${it.second} ${it.first}")
        }
        out.println()
        out.println("Распределение статей по размеру:")
        minMaxSizes.range?.let { intRange ->
            sorted(0, intRange, sizesText) {
                out.println("${it.first} ${it.second}")
            }
        }
        out.println()
        out.println("Распределение статей по времени:")
        minMaxYears.range?.let { intRange ->
            sorted(0, intRange, yearsText) {
                out.println("${it.first} ${it.second}")
            }
        }
    }
}
