import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.0"
    kotlin("plugin.serialization") version "1.6.0"
    id("org.jlleitschuh.gradle.ktlint") version "10.0.0" apply true
    application
}
group = "victor1234544.wiki"
version = "1.0-SNAPSHOT"

repositories {
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven") }
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.3")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.1")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.1")
    implementation("com.apurebase:arkenv:3.3.3")
    implementation("org.apache.commons:commons-compress:1.21")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:0.8.0")

    implementation(kotlin("reflect"))

    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    testImplementation(kotlin("test-junit5"))
}

ktlint {
    disabledRules.add("no-wildcard-imports")

    enableExperimentalRules.set(true)
    filter {
        exclude("**/resources/**")
        exclude("**/build/**")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
    kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
}

tasks.test {
    useJUnitPlatform()

    testLogging {
        outputs.upToDateWhen { false }
        showStandardStreams = true
    }
}

kotlin.sourceSets.all {
    languageSettings.apply {
        optIn("kotlin.time.ExperimentalTime")
    }
}
